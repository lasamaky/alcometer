import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private genders = [];
  private times = [];
  private bottles = [];
  private weight: number;
  private gender: string;
  private time: string;
  private bottle: string;
  private promilles: number;

    
      constructor() {}
    
    ngOnInit() {

      this.genders.push('Male');
      this.genders.push('Female');
    
      this.times.push('0');
      this.times.push('1');
      this.times.push('2');
      this.times.push('3');
      this.times.push('4');
      this.times.push('5');

      this.bottles.push('0');
      this.bottles.push('1');
      this.bottles.push('2');
      this.bottles.push('3');
      this.bottles.push('4');
      this.bottles.push('5');

    
    
      let factor = 0;

      switch (this.time) {
        case '0':
          factor = 0;
          break;
        case '1':
          factor = 1;
          break;
        case '2':
          factor = 2;
          break;
        case '3':
          factor = 3;
           break;
         case '4':
          factor = 4;
           break;
         case '5':
           factor = 5;
           break;
      }

      switch (this.bottle) {
        case '0':
          factor = 0;
          break;
        case '1':
          factor = 1;
          break;
        case '2':
          factor = 2;
          break;
        case '3':
          factor = 3;
           break;
         case '4':
          factor = 4;
           break;
         case '5':
           factor = 5;
           break;
      }
    }

    calculate() {
      if (this.gender === 'Male') {
        this.promilles = (( this.bottle * 0.33) * 8 * 4.5)/ (this.weight * 0.7);
      } else {
        this.promilles = ( this.bottle * 0.33 * 8 * 4.5)/ (this.weight * 0.6);
      }
    }
    
    
}
